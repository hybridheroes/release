#! /usr/bin/env node

import {join} from 'path';
import {cat, cp, echo, exec, ls, mkdir, rm, config} from 'shelljs';
import * as program from 'commander';
import * as chalk from 'chalk';

config.silent = false;
config.verbose = false;

const pkg = JSON.parse(cat(join(__dirname, '../package.json')));
const app = JSON.parse(cat('./package.json'));
const keychainFile: string = `~/Library/Keychains/release-${app.name}.keychain-db`;
// folder where the resulting packages can be found
const outputFolder = 'releases';
// folder where cordova puts builds for iOS
const iosBuildResult = 'platforms/ios/build/device';
// folder where cordova puts builds for Android
const androidBuildResult = 'platforms/android/build/outputs/apk';

// ios profile uuid
let iosProfileId: string;
// ios certificate name
let iosCertificateName: string;
let iosTeamId: string;
let iosPackageType: string;

const info = (message:string) => {
  console.log(chalk.yellow(message));
}
const error = (message:string) => {
  console.log(chalk.red(message));
}
const success = (message:string) => {
  console.log(chalk.green(message));
}
const exit = () => {
  error('Aborting ...');
  process.exit(1);
}

function execVerbose (command: string) {
  info(chalk.dim(command));
  return exec(command);
}

function checkRequirements(environment: any): boolean {
  let isGood: boolean = true;
  // check for name, version and description in package.json
  if (environment.name && environment.name.length > 0) {
    info(`package name: ${environment.name}`);
  } else {
    error(`package name ERROR ${environment.name}`);
    isGood = false;
  }
  if (environment.version &&
      environment.version.length > 0) {
        info(`package version: ${environment.version}`);
  } else {
    error(`package version ERROR! ${environment.version}`);
    isGood = false;
  }

  // check if ionic and cordova are installed as dependency
  if ('ionic' in environment.devDependencies) {
    info(`package ionic dependency: ${environment.devDependencies.ionic}`);
  } else {
    error(`package ionic dependency ERROR: ${JSON.stringify(environment.devDependencies)}`);
    isGood = false;
  }
  if ('cordova' in environment.devDependencies) {
    info(`package cordova dependency: ${environment.devDependencies.cordova}`);
  } else {
    error(`package cordova dependency ERROR: ${JSON.stringify(environment.devDependencies)}`);
    isGood = false;
  }

  // check if platform specs are available
  if (environment.cordovaPlatforms && environment.cordovaPlatforms instanceof Array && environment.cordovaPlatforms.length > 0) {
    info(`package cordovaPlatforms: ${environment.cordovaPlatforms}`);
  } else {
    error(`package cordovaPlatforms ERROR ${environment.cordovaPlatforms}`);
    isGood = false;
  }

  // check the android dependencies
  if (isGood && isAndroid(environment)) {
      info('Checking Android requirements');

    // check if we have max 1 keystore
    // we check this in the root of the project only!
    const kss = listKeystores().length;
    if (kss !== 1) {
      error('A single keystore file is required');
      isGood = false;
    }

    // check if there is a build.properties file and if so report an error.
    // we want to have all info through parameters instead for now
    const bjs = ls('build.json').length;
    if (bjs >= 1) {
      error('A build.json file must not be present');
      isGood = false;
    }

  }
  // check the ios dependencies
  if (isGood && isIos(environment)) {
      info('Checking iOS requirements');

    // check if we have max 1 mobileprovision and 1 p12 file
    // we check this in the root of the project only!
    const mps = ls('*.mobileprovision').length;
    if (mps !== 1) {
      error('A single mobileprovision file is required');
      isGood = false;
    }
    const p12s = ls('*.p12').length;
    if (p12s !== 1) {
      error('A single p12 file is required');
      isGood = false;
    }
  }

  if (isGood) {
    success('All requirements are satisfied');
  }
  return isGood;
};

function isAndroid(environment: any): boolean {
  return environment.cordovaPlatforms.find((platform: string) => platform.indexOf('android') === 0)
}

function isIos(environment: any): boolean {
  return environment.cordovaPlatforms.find((platform: string) => platform.indexOf('ios') === 0)
}

function listKeystores() {
  return [
    ...ls('*.keystore'),
    ...ls('*.jks')
  ];
}

// TODO: Automagically detect alias name
function getAliasName(keystore) {
  // keytool -v -list -keystore com.cashtocode.wettenpay.test.keystore -storepass 12345678a | grep Aliasname:
}

function cleanKeychain() {
  // remove the keychain if existing
  rm(keychainFile);
  // create a new keychain file and open it for processing
  if (execVerbose(`security create-keychain -p "${iosKeychainPassword}" ${keychainFile} || security unlock-keychain -p "${iosKeychainPassword}" ${keychainFile}`).code !== 0) {
    exit();
  }
  // disable automatic locking of keychain after timeout
  if (execVerbose(`security set-keychain-settings -u ${keychainFile}`).code !== 0) {
    exit();
  }
  // set keychain search list 
  if (execVerbose(`security list-keychains -s ${keychainFile}`).code !== 0) {
    exit();
  }
}

function configure(environment: any): boolean {
  let isGood = true;
  info('\nConfiguration step');

  if (isIos(environment)) {
      info('configure for iOS');
    // first handle mobileprovision files. They need to be copied to ~/Library/MobileDevice/Provisioning\ Profiles/ having as filename the uuid that can be extracted from its content
    const mp = ls('*.mobileprovision');
    if (mp.length === 1) {
      // we do this with normal grep as such:  grep UUID -A1 -a mobileprovisionfilename | grep -io "[-A-Z0-9]\{36\}"
      iosProfileId = execVerbose(`grep UUID -A1 -a ${mp[0]} | grep -io \"[-A-Z0-9]\\{36\\}\"`).toString().trim();
      success('mobileprovision uuid found: ' + iosProfileId);

      iosTeamId = execVerbose(`grep TeamIdentifier -A2 -a ${mp[0]} | grep -Ewo "[A-Z0-9]+"`).toString().trim();
      success('teamid found: ' + iosTeamId);

      // see https://github.com/ealeksandrov/ProvisionQL/blob/d363fe36ee7fec2464f0a97ce2c7b84b7899d4f0/ProvisionQL/GeneratePreviewForURL.m
      const getTaskAllow = execVerbose(`grep get-task-allow -A1 -a ${mp[0]} | grep -Ewo "(true|false)"`).toString().indexOf('true') !== -1;
      const hasDevices = execVerbose(`grep ProvisionedDevices -a ${mp[0]}`).toString().length > 0;
      const isEnterprise = execVerbose(`grep ProvisionsAllDevices -A1 -a ${mp[0]} | grep -Ewo "(true|false)"`).toString().indexOf('true') !== -1;
      if (hasDevices) {
        if (getTaskAllow) {
          iosPackageType = 'development';
        } else {
          iosPackageType = 'ad-hoc';
        }
      } else {
        if (isEnterprise) {
          iosPackageType = 'enterprise';
        } else {
          iosPackageType = 'app-store';
        }
      }
      success('package type: ' + iosPackageType);

      // we found a uuid. Install it.
      mkdir('-p', `~/Library/MobileDevice/Provisioning\ Profiles/`);      
      cp(mp[0], `~/Library/MobileDevice/Provisioning\ Profiles/`);
    }

    // second handle the p12 file if it exists
    const p12 = ls('*.p12');
    if (p12.length === 1) {
      // remove the keychain file we use to avoid possible problems
      cleanKeychain();

      // import the p12 file
      // https://github.com/fastlane/fastlane/blob/60bac7e4a2a6c1e19d1cd20eb83201e7d0cb9e48/fastlane_core/lib/fastlane_core/keychain_importer.rb
        info(`about to import p12: ${p12[0]}, with password: "${iosP12Password}" to keychain file: ${keychainFile}`);
      execVerbose(`security import "${p12[0]}" -k ${keychainFile} -P "${iosP12Password}" -T /usr/bin/codesign -T /usr/bin/security`);
      // prevent permission dialog from popping up when codesign is trying to access key http://stackoverflow.com/questions/39868578/security-codesign-in-sierra-keychain-ignores-access-control-settings-and-ui-p
      if (execVerbose(`security set-key-partition-list -S apple-tool:,apple: -s -k "${iosKeychainPassword}" ${keychainFile}`).code !== 0) {
        exit();
      }



      // we can now find the code signing identity from the p12 that we just added to the keychain.
      // we know that there is just one which makes finding it a little easier
      const identities = execVerbose(`security find-identity -v -p codesigning ${keychainFile}`);
      success('identities found: ' + identities);
      // take the first line, and get the third word without the hashes
      const identitiesTemp: string = identities.toString().match(/\".*\"/).toString();
      const identity = identitiesTemp.substring(1, identitiesTemp.length - 1);
      if (identity.length <= 0) {
        error('ERROR not finding identity for signing');
        isGood = false;
      } else {
        iosCertificateName = identity;
        success('found identity: ' + iosCertificateName);
      }

    }
  }
  if (isAndroid(environment)) {
      info('configure for Android');
  }

  return isGood;
}

function prerelease(environment: any): boolean {
  let isGood = true;
  info('\nPrerelease step');

  // prefer not to have untracked changes with every release
  // if (execVerbose('ionic resources').code !== 0) {
  //   exit();
  // }
  if (execVerbose('ionic state reset').code !== 0) {
    exit();
  }

  return isGood;
}

function release(environment: any): boolean {
  let isGood = true;
  info('\nRelease step');

  if (isIos(app)) {
    // generate ios package
    if (execVerbose(`node_modules/cordova/bin/cordova build ios --verbose --device --release --codeSignIdentity="${iosCertificateName}" --provisioningProfile="${iosProfileId}" --developmentTeam="${iosTeamId}" --packageType="${iosPackageType}"`).code !== 0) {
      exit();
    }
  }

  if (isAndroid(app)) {
    // get keystore location
    const keystore = listKeystores()[0];
      info(`release using android keystore: ${keystore}, storePassword: ${androidStorePassword}, alias: ${androidAlias}, password: ${androidPassword}`);

    if (execVerbose(`node_modules/cordova/bin/cordova build android --device --release --verbose -- --keystore=./${keystore} --storePassword="${androidStorePassword}" --alias="${androidAlias}" --password="${androidPassword}"`).code !== 0) {
      exit();
    }
  }

  return isGood;
}

function postrelease(environment: any): boolean {
  let isGood = true;
  info('\nPostrelease step');

  // create output folder when it does not yet exist
  mkdir('-p', outputFolder);

  if (isIos(app)) {
    // copy the resulting ipa file to the output folder
    if (cp(`${iosBuildResult}/*.ipa`, `${outputFolder}/${app.name}-${app.version}.ipa`).code !== 0) {
      exit();
    };
  }

  if (isAndroid(app)) {
    // copy the resulting apk file to the output folder
    ls(`${androidBuildResult}/*-release.apk`).forEach(apkFile => {
      let arch = apkFile.replace(/(.*(\-armv7|\-x86|(?=android\-release)).*)/, '$2');
      if (cp(`${apkFile}`, `${outputFolder}/${app.name}-${app.version}${arch}.apk`).code !== 0) {
        exit();
      }
    })
  }

  // TODO: cleanup, e.g. removing keychain

  return isGood;
}

// HERE 'MAIN' starts
info(`RELEASE ${pkg.version}`);

program
  .version(pkg.version)
  .usage('[options]');

if (isIos(app)) {
  program.option('-p, --p12-password <password>', 'iOS private key password')
}

if (isAndroid(app)) {
  program
    .option('-A, --alias-name <name>', 'Android keystore alias name')
    .option('-a, --alias-password <password>', 'Android keystore alias password')
    .option('-k, --keystore-password <password>', 'Android keystore password');
}


interface ReleaseOptions extends program.ICommand {
  p12Password?: string
  aliasName?: string
  aliasPassword?: string
  keystorePassword?: string
}

let options: ReleaseOptions = program.parse(process.argv);

if (isIos(app)) {
  if (!options.p12Password) {
    error('Missing parameters for iOS platform');
    program.outputHelp();
    exit();
  }
}

if (isAndroid(app)) {
  if (!options.aliasName || !options.aliasPassword || !options.keystorePassword) {
    error('Missing parameters for Android platform');
    program.outputHelp();
    exit();
  }
}

const iosKeychainPassword = 'release';
const iosP12Password = options.p12Password;
const androidStorePassword = options.keystorePassword;
const androidAlias = options.aliasName;
const androidPassword = options.aliasPassword;

if(checkRequirements(app)) {
  // we can continue with configuration
  if (configure(app)) {
    success('Configuration successful');
    if (prerelease(app)) {
      success('Prerelease successful');
      if (release(app)) {
        success('Release successful');
        if (postrelease(app)) {
          success('Postrelease successful');
        } else {
          error('Postrelease ERROR');
        }
      } else {
        error('Release ERROR');
      }
    } else {
      error('Prerelease ERROR');
    }
  } else {
    error('Configuration ERROR');
  }
} else {
error('Error while checking requirements. Stopping');
}
