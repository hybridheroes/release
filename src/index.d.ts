// shelljs @types are stuck on v0.3.0
declare module 'shelljs' {
  let config: {
    verbose: boolean;
    silent: boolean;
  }
  function cp(from: string, to: string)
  function cat(path: string): string
  function exec(command: string, options?: {async: boolean, silent: boolean}, callback?: () => {}): {code: number, stdout: string, stderr: string}
  function ls(path: string): string[]
  function mkdir(options: string, path: string)
  function echo(text: string)
  function rm(path: string)
}
